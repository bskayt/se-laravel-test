<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use OpenApi\Annotations as OA;

/**
 * A common scenario is to let swagger-php generate a definition based on your model class.
 * These definitions can then be referenced with `ref="#/components/schemas/$classname".
 */

class Note extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'text'
    ];  

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
