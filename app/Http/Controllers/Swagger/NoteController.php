<?php

namespace App\Http\Controllers\Swagger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class NoteController extends Controller
{
    /**
     * 
     * @OA\Get(
     *     path="/api/note",
     *     summary="Выбор записей пользователя",
     *     security={{"bearerAuth":{}}},
     *     tags={"Note"},
    *  @OA\Parameter(
    *         description="id пользователя",
    *         in="query",
    *         name="user_id",
    *         required=false,
    *         @OA\Schema(type="integer")
    *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={"success": true , "data" : {{ "id": 113, "text": "some random text 222", "user_id": 2, "created_at": "2023-11-15T16:57:47.000000Z", "updated_at": "2023-11-15T16:57:47.000000Z", "deleted_at": null}}, "message": ""}, summary="An result object.")
     *         )
     *     )
     * )
     * 
     * 
     * 
       * @OA\Post(
     *     path="/api/note",
     *     summary="Добавление записи",
     *     security={{"bearerAuth":{}}},
     *     tags={"Note"},
    *      @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(
 *                     property="text",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="user_id",
 *                     type="integer"
 *                 ),
 *                 example={ "text": "some random text 222","user_id": 3 }
 *             )
 *         )
 *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={
    * "success": true,
    * "data": {
   *      "text": "some random text 222",
  *       "user_id": 1,
  *       "updated_at": "2023-11-16T20:11:31.000000Z",
  *       "created_at": "2023-11-16T20:11:31.000000Z",
  *       "id": 127
  *   },
  *   "message": "note created"
* }, summary="An result object.")
     *         )
     *     )
     * )
     * 
     * 
     * 
 * @OA\Put(
 *     path="/api/note/{id}",
 *     summary="Обновление записи",
 *     security={{"bearerAuth":{}}},
 *     tags={"Note"},
 *     @OA\Parameter(
 *         description="Id note",
 *         in="path",
 *         name="id",
 *         required=true,
 *         @OA\Schema(type="integer"),
 *     ),
 *    @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(
 *                     property="text",
 *                     type="string"
 *                 ),
 *                 example={"text": "a3fb6"}
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="OK",
 *              @OA\JsonContent(
     *             @OA\Examples(example="result", 
     *  {"success":true,"data":{"id":10,"text":"ne dushi","user_id":3,"created_at":"2023-11-15T14:18:28.000000Z","updated_at":"2023-11-16T18:13:40.000000Z","deleted_at":null},"message":"Note success update"}
     * , summary="An result object.")
     *         )
 *     )
 * )
 * 
 * 
  * @OA\Delete(
 *     path="/api/note/{id}",
 *     summary="Удаление записи",
 *     security={{"bearerAuth":{}}},
 *     tags={"Note"},
 *     @OA\Parameter(
 *         description="Id note",
 *         in="path",
 *         name="id",
 *         required=true,
 *         @OA\Schema(type="integer"),
 *     ),
 * 
 *     @OA\Response(
 *         response=200,
 *         description="OK",
 *              @OA\JsonContent(
     *             @OA\Examples(example="result", 
*           {"success":true,"data":{},"message":"Note deleted"}
     * , summary="An result object.")
     *         )
 *     )
 * )
 * 
 
        * @OA\Post(
     *     path="/api/register",
     *     summary="Register",
     *     tags={"User"},
    *      @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(
 *                     property="email",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="password",
 *                     type="string"
 *                 ),
 *                 example={ "email": "admin@larav.el","password": "password" }
 *             )
 *         )
 *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={"success":true,"data":{"token":"api_token"},"message":"Login success"}, summary="An result object.")
     *         )
     *     )
     * ) 
  
  
       * @OA\Post(
     *     path="/api/login",
     *     summary="Login",
     *     tags={"User"},
    *      @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(
 *                     property="name",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="email",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="password",
 *                     type="string"
 *                 ),
 *                 example={ "email": "admin@larav.el","password": "password" }
 *             )
 *         )
 *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={"success":true,"data":{"token":"api_token"},"message":"Login success"}, summary="An result object.")
     *         )
     *     )
     * ) 
    
    
     * @OA\Get(
     *     path="/api/users",
     *     summary="Выбор всех пользователей",
     *     security={{"bearerAuth":{}}},
     *     tags={"User"},
    *  
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={"success":true,"data":{"token":"api_token"},"message":"Login success"}, summary="An result object.")
     *         )
     *     )
     * )
     */
    public function index(){

    }
}
