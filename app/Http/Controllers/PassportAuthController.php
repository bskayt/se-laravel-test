<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class PassportAuthController extends BaseApiController
{
    /**
     * Registration Req
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:4',
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);
  
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
  
        $token = $user->createToken('pass')->accessToken;
        
        return $this->sendResponse(['token' => $token],'User registered');
        //return response()->json(['token' => $token], 200);
    }
  
    /**
     * Login Req
     */
    public function login(Request $request)
    {
        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];
  
        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken('pass')->accessToken;
            return $this->sendResponse(['token' => $token],'Login success');
            //return response()->json(['token' => $token], 200);
        } else {
            return $this->sendError('No access',[],403); 
            //return response()->json(['error' => 'Unauthorised'], 401);
        }
    }
 
    public function userList() 
    {
        return $this->sendResponse(User::all(),'User list');
    }
}
