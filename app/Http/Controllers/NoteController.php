<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoteStoreRequest;
use App\Models\Note;
use App\Models\User;
use App\Providers\NoteNotify;
use Dotenv\Validator as DotenvValidator;
use Exception;
use Illuminate\Http\Request;


class NoteController extends BaseApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $user = null;
        if (auth()->user()->is_admin && request()->filled('user_id')) {
            $user = User::find((int)request()->user_id);
            if ($user === null) {
                return $this->sendError('User not Found');  
            }
        }
        else {
            $user = auth()->user();
        }
        $notes = $user->notes()->get();
        return $this->sendResponse($notes,'');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(NoteStoreRequest $request)
    {
        $data = $request->validated();
        
        $user = auth()->user();
        //dd($user);
        if ($user->is_admin && !empty($data['user_id']))
        {
            $user = User::where('id',$data['user_id'])->first();
            if ($user === null) {
                return $this->sendError('User not Found');  
            }
        } 
        $note = $user->notes()->create($data);
        $note->save();
        event(new NoteNotify($note));
        return $this->sendResponse($note,'note created');
    }

    /**
     * Display the specified resource.
     */
    /*
    public function show(string $id)
    {
        if (!(auth()->user()->is_admin || auth()->user()->id == $id)) {
            return $this->sendError('No access',[],403); 
        }
        $notes = Note::where('user_id',$id)->get();
        return $this->sendResponse($notes,'');
    }
    */

    /**
     * Update the specified resource in storage.
     */
    public function update(NoteStoreRequest $request, string $id)
    {
        $data = $request->validated();
        try {
            $note = $this->getNoteByUserAccess($id);
        }
        catch (Exception $ex)
        {
            return $this->sendError($ex->getMessage(),[],$ex->getCode()); 
        }
        $note->update(['text' => $data['text']]);
        return $this->sendResponse($note,'Note success update');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $note = $this->getNoteByUserAccess($id);
        }
        catch (Exception $ex)
        {
            return $this->sendError($ex->getMessage(),[],$ex->getCode()); 
        }
        $note->delete();
        return $this->sendResponse([],'Note deleted');
    }

    private function getNoteByUserAccess(string $id){
        $user = auth()->user();
        $note = Note::find($id);
        if ($note === null) {
            throw new Exception('Note not Found', 404);
        }
        if (!$user->is_admin && $user->id != $note->user_id) {
            throw new Exception('No access',403);
        }
        return $note;
    }
}
