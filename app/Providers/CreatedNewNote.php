<?php

namespace App\Providers;

use App\Mail\NoteCreateNotify;
use App\Models\User;
use App\Providers\NoteNotify;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use \Symfony\Component\Console\Output\ConsoleOutput;

class CreatedNewNote
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
    * Handle the event.
    */
    public function handle(NoteNotify $event): void
    {
        //
        $note = $event->note;
        $output = new ConsoleOutput();
        $output->writeln("<info>{$note->text}</info>");
        
        Mail::to(env('MAIL_FROM_ADDRESS'))->queue(new NoteCreateNotify($note));
    }
}
