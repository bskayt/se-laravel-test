<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@larav.el',
            'password' => Hash::make('password'),
            'email_verified_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'is_admin' => 1,
        ]);

        DB::table('users')->insert([
            'name' => 'user',
            'email' => 'user@larav.el',
            'password' => Hash::make('password'),
            'email_verified_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'is_admin' => 0,
        ]);
    }
}
