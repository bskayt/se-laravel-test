## Развертывание приложения
Клонируем репозиторий
```
$ git clone https://gitlab.com/bskayt/se-laravel-test.git
```
Необходимо подтянуть библиотеки ларавель
```
$ composer install
```
В папке с проектом нужно создать .env-файл и сгенерировать ключ
```
$ cp .env.example .env
$ php artisan key:generate
```
В файле .env необходимо установить параметры подключения к БД
```
DB_CONNECTION=
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
``` 
И параметры подключения к mail серверу
```
MAIL_MAILER=
MAIL_HOST=
MAIL_PORT=
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=
MAIL_FROM_ADDRESS=
```
Создание таблиз в БД
```
$ php artisan migrate
$ php artisan passport:install
```
Заполнение БД записями
```
$ php artisan db:seed
```
Для запуска комманда
```
$ php artisan serve
```
Swagger
```
/api/documentation/
```
## Тестовые пользователи
Администратор
```
email: admin@larav.el
pw: password
```
Пользователь
```
email: user@larav.el
pw: password
```