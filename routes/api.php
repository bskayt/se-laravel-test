<?php

use App\Http\Controllers\BaseApiController;
use App\Http\Controllers\NoteController;
use App\Http\Controllers\PassportAuthController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::middleware('api')->group(function () {
    Route::middleware('auth:api')->group(function () {
        Route::resource('note', NoteController::class)->except(['show']);

        Route::get('users', [PassportAuthController::class, 'userList'])->middleware('admin');
    });

    

    Route::post('register', [PassportAuthController::class, 'register']);
    Route::post('login', [PassportAuthController::class, 'login']);
    
    Route::any('/{any?}', [BaseApiController::class, 'error'])->where('any', '.*')->name('any');
});


